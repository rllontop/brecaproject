<header class="page-header" role="banner">
    <!-- DOC: nav menu layout change shortcut -->
    <div class="hidden-md-down dropdown-icon-menu position-relative">
        <a href="#" class="header-btn btn js-waves-off" data-action="toggle" data-class="nav-function-hidden"
            title="Hide Navigation">
            <i class="ni ni-menu"></i>
        </a>
        <ul>
            <li>
                <a href="#" class="btn js-waves-off" data-action="toggle" data-class="nav-function-minify"
                    title="Minify Navigation">
                    <i class="ni ni-minify-nav"></i>
                </a>
            </li>
            <li>
                <a href="#" class="btn js-waves-off" data-action="toggle" data-class="nav-function-fixed"
                    title="Lock Navigation">
                    <i class="ni ni-lock-nav"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- DOC: mobile button appears during mobile width -->
    <div class="hidden-lg-up">
        <a href="#" class="header-btn btn press-scale-down bg-transparent text-secondary border-secondary" data-action="toggle" data-class="mobile-nav-on" style="background-image: none;">
            <i class="ni ni-menu"></i>
        </a>
    </div>

    <div class="ml-auto d-flex">
        <!-- app user menu -->
        <div>
            <a href="#" data-toggle="dropdown" title="example@domain.com"
                class="header-icon d-flex align-items-center justify-content-center ml-2">
                <img src="img/demo/avatars/avatar-m.png" class="profile-image rounded-circle"
                    alt="User">
                <!-- you can also add username next to the avatar with the codes below:
                    <span class="ml-1 mr-1 text-truncate text-truncate-header hidden-xs-down">Me</span>
                    <i class="ni ni-chevron-down hidden-xs-down"></i> -->
            </a>
            <div class="dropdown-menu dropdown-menu-animated dropdown-lg rounded">
                <div class="dropdown-header bg-trans-gradient d-flex flex-row py-4 rounded-top">
                    <div class="d-flex flex-row align-items-center mt-1 mb-1 color-white">
                        <span class="mr-2">
                            <img src="img/demo/avatars/avatar-m.png" class="rounded-circle profile-image"
                                alt="User">
                        </span>
                        <div class="info-card-text">
                            <div class="fs-lg text-truncate text-truncate-md" data-toggle="tooltip" data-placement="top"
                                data-original-title="User">User</div>
                            <span class="text-truncate text-truncate-md opacity-80">example@domain.com</span>
                        </div>
                    </div>
                </div>
                <div class="dropdown-divider m-0"></div>
                {{-- <a href="#" class="dropdown-item" data-toggle="modal" data-target=".js-modal-settings">
                    <span data-i18n="drpdwn.settings">Configuración</span>
                </a> --}}
                <div class="dropdown-divider m-0"></div>
                <a href="#" class="dropdown-item" data-action="app-fullscreen">
                    <span class="fal fa-expand mr-1"></span>
                    <span data-i18n="drpdwn.fullscreen">Pantalla Completa</span>
                    <i class="float-right text-muted fw-n">F11</i>
                </a>
                <a href="#" class="dropdown-item" data-action="app-print">
                    <span class="fal fa-print mr-1"></span>
                    <span data-i18n="drpdwn.print">Imprimir</span>
                    <i class="float-right text-muted fw-n">Ctrl + P</i>
                </a>
                <div class="dropdown-multilevel dropdown-multilevel-left">
                    <div class="dropdown-item">
                        <span class="fal fa-language mr-1"></span>
                        <span data-i18n="drpdwn.language">Idioma</span>
                    </div>
                    <div class="dropdown-menu">
                        <a href="#?lang=en" class="dropdown-item" data-action="lang" data-lang="en">English (US)</a>
                        <a href="#?lang=es" class="dropdown-item active" data-action="lang" data-lang="es">Español (ES)</a>
                        {{-- <a href="#?lang=fr" class="dropdown-item" data-action="lang" data-lang="fr">Français</a>
                        <a href="#?lang=ru" class="dropdown-item" data-action="lang" data-lang="ru">Русский язык</a>
                        <a href="#?lang=jp" class="dropdown-item" data-action="lang" data-lang="jp">日本語</a>
                        <a href="#?lang=ch" class="dropdown-item" data-action="lang" data-lang="ch">中文</a> --}}
                    </div>
                </div>
                <div class="dropdown-divider m-0"></div>
                <a class="dropdown-item fw-500 pt-3 pb-3 rounded-bottom text-danger" href="#"
                    onclick="//event.preventDefault();document.getElementById('logout-form').submit();">
                    <span class="fal fa-power-off mr-1"></span>
                    <span data-i18n="drpdwn.page-logout">#</span>
                </a>
                <form id="logout-form" action="#" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</header>