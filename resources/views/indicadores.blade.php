@extends('layouts.app')

@section('css')
<link rel="stylesheet" media="screen, print" href="{{asset('css/datagrid/datatables/datatables.bundle.css')}}">
<link rel="stylesheet" media="screen, print"
  href="{{asset('css/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.css')}}">
<link rel="stylesheet" media="screen, print" href="css/formplugins/select2/select2.bundle.css">
@endsection

@section('content')
<div class="d-flex justify-content-between mb-3">
  <ol class="breadcrumb breadcrumb-arrow">
    <li class=""><a href="javascript:void(0);" class=""><span>Analytics</span></a></li>
    <li class="active"><a href="javascript:void(0);" class=""><span>Dashboard</span></a></li>
  </ol>
  <ol class="breadcrumb breadcrumb-arrow">
    <li class="d-none d-sm-block"><a href="javascript:void(0);" class="bg-transparent"><span
          class="js-get-date color-fusion-100"></span></a></li>
  </ol>
</div>
<div class="subheader">
  <h1 class="subheader-title">
    <i class='subheader-icon fal fa-chart-area'></i> Indicadores <span class='fw-300'>Dashboard</span>
  </h1>
</div>
<div class="row">
  <div class="col-12">
    <div class="panel">
      <div class="panel-hdr">
        <h2>Parámetros</h2>
        <div class="panel-toolbar">
          <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Collapse"></button>
          <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Fullscreen"></button>
          <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Close"></button>
        </div>
      </div>
      <div class="panel-container">
        <div class="panel-content row">
          <div class="form-group col-12 col-md-6">
            <label class="form-label">Marca:</label>
            <select name="tenant_id" id="tenant_id" class="select2-placeholder">
              <option value="">TODOS</option>
              @foreach ($tenants as $tenant)
              <option value="{{$tenant->id}}">{{$tenant->locatario}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="panel">
      <div class="panel-hdr">
        <h2>Likes de redes sociales por Marca</h2>
        <div class="panel-toolbar">
          <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Collapse"></button>
          <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Fullscreen"></button>
          <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
            data-original-title="Close"></button>
        </div>
      </div>
      <div class="panel-container show row">
        <div class="col-12 col-md-4">
          <div class="panel-content">
            <table id="sale-by-area-table"
              class="table responsive table-bordered table-hover table-striped w-100 dataTable dtr-inline collapsed">
            </table>
          </div>
        </div>
        <div class="col-12 col-md-8">
          <div class="panel-content">
            <div class="panel-tag shadow">
              <h4>Top 5 - Likes por Marca</h4>
            </div>
            <div id="pie-container-likes" class="panel-content show p-4">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6">
        <div class="panel">
          <div class="panel-hdr">
            <h2>Visitantes por género</h2>
            <div class="panel-toolbar">
              <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Collapse"></button>
              <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Fullscreen"></button>
              <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Close"></button>
            </div>
          </div>
          <div class="panel-container">
            <div id="pie-container-genero" class="panel-content show p-4">
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="panel">
          <div class="panel-hdr">
            <h2>Top 10 visitantes por edad</h2>
            <div class="panel-toolbar">
              <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Collapse"></button>
              <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Fullscreen"></button>
              <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                data-original-title="Close"></button>
            </div>
          </div>
          <div class="panel-container">
            <div id="pie-container-edad" class="panel-content show p-4">
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/datagrid/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/datagrid/datatables/datatables.export.js')}}"></script>
<script src="{{asset('js/dependency/moment/moment.js')}}"></script>
<script src="{{asset('js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.js')}}"></script>
<script src="{{asset('js/statistics/flot/flot.bundle.js')}}"></script>
<script src="{{asset('js/formplugins/select2/select2.bundle.js')}}"></script>
<script>
  $(document).ready(function () {

        var tableOne = null;

        function appendLeadingZeroes(n){
            if(n <= 9){
                return "0" + n;
            }
            return n
        }

        var select2 = $(".select2-placeholder").select2();

        var plotNow = function(data_pie_genero,data_pie_edad,data_pie_likes) {
            $('#pie-container-genero').html('<div id="js-pie-options-genero" class="w-100" style="height: 350px"></div>');
            $('#pie-container-edad').html('<div id="js-pie-options-edad" class="w-100" style="height: 350px"></div>');
            $('#pie-container-likes').html('<div id="js-pie-options-likes" class="w-100" style="height: 350px"></div>');
            var labelFormatter = function(label, series) {
                return "<div class='fs-xs text-center p-1 text-white'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
            }
            var option_pie = {
              series: {
                pie: {
                  show: true,
                  radius: 1,
                  label: {
                    show: true,
                    radius: 1,
                    formatter: labelFormatter,
                    background: {
                      opacity: 0.5,
                      color: "#000"
                    }
                  }
                }
              },
              legend: {
                show: false
              }
            };
            var option_deafult = {
              series: {
                pie: {
                  show: true
                }
              }
            };
            var plot1 = $.plot($("#js-pie-options-genero"), data_pie_genero, option_pie);
            var plot2 = $.plot($("#js-pie-options-likes"), data_pie_likes, option_deafult); 
            var plot3 = $.plot($("#js-pie-options-edad"), data_pie_edad, option_pie); 
            
        };
      
        tableOne = $('#sale-by-area-table').DataTable({
            processing: true,
            responsive: true,
            pageLength: number_of_pages,
            pagingType: "full_numbers",
            ajax: {
                url: "{{route('like.by.brand')}}",
                dataType: "json",
                type: "GET",
                data: function (d) {
                    d.brand = $('select[name="tenant_id"] option[value="'+$('select[name="tenant_id"]').val()+'"]').text()
                    // d.endDate = fecha_final
                }
            },
            columns:[
                {data: "locatario",title: "Marca",className: "all w-25"},
                {data: "counta",title: "Likes"},
            ],
            buttons: [
                /*{
                    extend:    'colvis',
                    text:      'Column Visibility',
                    titleAttr: 'Col visibility',
                    className: 'mr-sm-3'
                },*/
                {
                    extend: 'pdfHtml5',
                    text: 'PDF',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1'
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1'
                },
                {
                    extend: 'print',
                    text: 'Print',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-primary btn-sm'
                }
            ],
            // buttons: botones("ventas_por_area","visible",['csv','pdf']),
            language: lenguaje,
            initComplete: function( settings, json ) {
                plotNow(json.pie_info_genero,json.pie_info_edad,json.pie_info_likes);
            },
            dom: dom_table(true),
            order: [[1,'desc']]
        });
        var headerTableOne = $(tableOne.table().header());
        var trHtml = '';
        headerTableOne.find('tr:eq(0) th').each(function (i) {
            // console.log(i);
           var title =$(this).text();
           trHtml += '<th><input type="text" class="form-control form-control-sm" placeholder="Buscar ' + title + '" /></th>';
        }).closest('thead').addClass('bg-highlight').append('<tr>'+trHtml+'</tr>').find("th input[type=text]").on('keyup change', function (i) {
            // console.log(i);
            tableOne.column($(this).parent().index()+':visible').search(this.value).draw(); 
        });

        $('#tenant_id').on('select2:select',function (e) {
            // console.log($(this).val());
            // var searchTerm = $(this).val(), regex = '\b' + searchTerm + '\b';
            // tableOne.columns(0).search(searchTerm,true,false).draw();
            // console.log(tableOne.rows( {search:'applied'} ).data());
            tableOne.ajax.reload(function (json) {
              plotNow(json.pie_info_genero,json.pie_info_edad,json.pie_info_likes);
            });

        });
    });
</script>
@endsection