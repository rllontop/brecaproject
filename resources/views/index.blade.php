@extends('layouts.app')

@section('css')
<link rel="stylesheet" media="screen, print" href="{{asset('css/datagrid/datatables/datatables.bundle.css')}}">
<link rel="stylesheet" media="screen, print"
    href="{{asset('css/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.css')}}">
<link rel="stylesheet" media="screen, print" href="css/formplugins/select2/select2.bundle.css">
@endsection

@section('content')
<div class="d-flex justify-content-between mb-3">
    <ol class="breadcrumb breadcrumb-arrow">
        <li class=""><a href="javascript:void(0);" class=""><span>Analytics</span></a></li>
        <li class="active"><a href="javascript:void(0);" class=""><span>Dashboard</span></a></li>
    </ol>
    <ol class="breadcrumb breadcrumb-arrow">
        <li class="d-none d-sm-block"><a href="javascript:void(0);" class="bg-transparent"><span
                    class="js-get-date color-fusion-100"></span></a></li>
    </ol>
</div>
<div class="subheader">
    <h1 class="subheader-title">
        <i class='subheader-icon fal fa-chart-area'></i> Analytics <span class='fw-300'>Dashboard</span>
    </h1>
    <div class="subheader-block d-lg-flex align-items-center">
        <div class="d-inline-flex flex-column justify-content-center mr-3">
            <span class="fw-300 fs-xs d-block opacity-50">
                <small>TOTAL DE VENTAS</small>
            </span>
            <span id="total-sales" class="fw-500 fs-xl d-block color-primary-500">
            </span>
        </div>
    </div>
    <div class="subheader-block d-lg-flex align-items-center border-faded border-right-0 border-top-0 border-bottom-0 ml-3 pl-3">
        <div class="d-inline-flex flex-column justify-content-center mr-3">
            <span class="fw-300 fs-xs d-block opacity-50">
                <small>TICKET PROMEDIO</small>
            </span>
            <span id="avg-ticket" class="fw-500 fs-xl d-block color-danger-500">
            </span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="panel">
            <div class="panel-hdr">
                <h2>Ventas por Área por Fecha</h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Collapse"></button>
                    <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                        data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container">
                <div class="panel-content row">
                    <div class="form-group col-12 col-md-6">
                        <label class="form-label">Rango de fechas:</label>
                        <input type="text" class="form-control" id="datepicker-1" name="rango_fechas"
                            placeholder="Select date">
                    </div>
                    <div class="form-group col-12 col-md-3">
                        <label class="form-label">Locatario:</label>
                        <select name="tenant_id" id="tenant_id" class="select2-placeholder">
                            <option value="">TODOS</option>
                            @foreach ($tenants as $tenant)
                            <option value="{{$tenant->id}}">{{$tenant->locatario}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-3">
                        <label class="form-label">Centro Comercial:</label>
                        <select name="venue_id" id="venue_id" class="select2-placeholder">
                            <option value="">TODOS</option>
                            @foreach ($venues as $venue)
                            <option value="{{$venue->id}}">{{$venue->centrocomercial}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="panel-content py-2 rounded-bottom border-faded border-left-0 border-right-0">
                    <div id="flot-toggles" class="w-100 mt-4" style="height: 300px"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="panel">
                    <div class="panel-hdr">
                        <h2>Top 10 por Área</h2>
                        <div class="panel-toolbar">
                            <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Collapse"></button>
                            <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Fullscreen"></button>
                            <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Close"></button>
                        </div>
                    </div>
                    <div id="pie-container-area" class="panel-container show p-4">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="panel">
                    <div class="panel-hdr">
                        <h2>Top 10 por Locatario</h2>
                        <div class="panel-toolbar">
                            <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Collapse"></button>
                            <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Fullscreen"></button>
                            <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip"
                                data-offset="0,10" data-original-title="Close"></button>
                        </div>
                    </div>
                    <div id="pie-container-locatario" class="panel-container show p-4">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-hdr">
                <h2>Ventas por Área</h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Collapse"></button>
                    <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                        data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container show">

                <div class="panel-content">
                    <table id="sale-by-area-table"
                        class="table responsive table-bordered table-hover table-striped w-100 dataTable dtr-inline collapsed">
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/datagrid/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/datagrid/datatables/datatables.export.js')}}"></script>
<script src="{{asset('js/dependency/moment/moment.js')}}"></script>
<script src="{{asset('js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.js')}}"></script>
<script src="{{asset('js/statistics/flot/flot.bundle.js')}}"></script>
<script src="{{asset('js/formplugins/select2/select2.bundle.js')}}"></script>
<script>
    $(document).ready(function () {

        var fecha_inicial = null;
        var fecha_final = null;
        var tableOne = null;
        var plot1 = null;
        var plot2 = null;
        var plot3 = null;

        function appendLeadingZeroes(n){
            if(n <= 9){
                return "0" + n;
            }
            return n
        }

        var select2 = $(".select2-placeholder").select2({
            // placeholder: "TODOS",
            // allowClear: true
        });//.val(null).trigger('change');

        var plotNow = function(data_chart,data_pie_area,data_pie_locatario) {
            var info_chart = [{
                // label: "Target Profit",
                data: data_chart,
                color: color.primary._600,
                bars:
                {
                    show: true,
                    align: "center",
                    barWidth: 30 * 30 * 60 * 10 * 80,
                    lineWidth: 0,
                    /*fillColor: {
                        colors: [color.primary._500, color.primary._900]
                    },*/
                    fillColor:
                    {
                        colors: [
                        {
                            opacity: 0.9
                        },
                        {
                            opacity: 0.9
                        }]
                    }
                },
                highlightColor: 'rgba(255,255,255,0.2)',
                shadowSize: 0
            }];
            var options_chart = {
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false
                },
                xaxis:
                {
                    mode: "time"
                },
                yaxes:
                {
                    // tickFormatter: function(val, axis)
                    // {
                    //     return "$" + val;
                    // },
                    // max: 1200
                }

            };
            if (plot2) {
                plot2.setData(info_chart);
                plot2.draw();
            } else {
                plot2 = $.plot($("#flot-toggles"), info_chart, options_chart);
            }
            $('#pie-container-area').html('<div id="js-pie-options-area" class="w-100" style="height: 350px"></div>');
            $('#pie-container-locatario').html('<div id="js-pie-options-locatario" class="w-100" style="height: 350px"></div>');
            var labelFormatter = function(label, series) {
                return "<div class='fs-xs text-center p-1 text-white'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
            }
            var option_pie = {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        label: {
                            show: true,
                            radius: 1,
                            formatter: labelFormatter,
                            background: {
                                opacity: 0.5,
                                color: "#000"
                            }
                        }
                    }
                },
                legend: {
                    show: false
                }
            };
            plot1 = $.plot($("#js-pie-options-area"), data_pie_area, option_pie);
            plot3 = $.plot($("#js-pie-options-locatario"), data_pie_locatario, option_pie);
            
        };

        var datarange = $('#datepicker-1').daterangepicker({
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            // "timePicker": true,
            // "timePicker24Hour": true,
            // "timePickerSeconds": true,
            // "autoApply": true,
            // "maxSpan": {"days": 7},
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                format: 'DD/MM/YYYY'
            },
            "alwaysShowCalendars": false,
            "startDate": moment().subtract(1, 'month'),
            "endDate": moment(),
            "applyButtonClasses": "btn-default shadow-0",
            "cancelClass": "btn-success shadow-0"
        }, function(start, end, label) {
            fecha_inicial = start.format('YYYY-MM-DD');
            fecha_final = end.format('YYYY-MM-DD');
            tableOne.ajax.reload(function (json) {
                plotNow(json.chart_info,json.pie_info_area,json.pie_info_locatario);
                $('#total-sales').text("S/. "+json.total_sales);
                $('#avg-ticket').text(json.avg_ticket);
            });
            
            // console.log('New date range selected: ' + fecha_inicial + ' to ' + fecha_final + ' (predefined range: ' + label + ')');
            // return {startDate:start,endDate:end};
        });



        var startDate = datarange.data('daterangepicker').startDate._d;
        var endDate = datarange.data('daterangepicker').endDate._d;
        fecha_inicial =  startDate.getFullYear() + "-"+ appendLeadingZeroes(startDate.getMonth() + 1) + "-" + appendLeadingZeroes(startDate.getDate());
        fecha_final = endDate.getFullYear() + "-"+ appendLeadingZeroes(endDate.getMonth() + 1) + "-" + appendLeadingZeroes(endDate.getDate());

        tableOne = $('#sale-by-area-table').DataTable({
            processing: true,
            responsive: true,
            pageLength: number_of_pages,
            pagingType: "full_numbers",
            ajax: {
                url: "{{route('sale.by.area')}}",
                dataType: "json",
                type: "GET",
                data: function (d) {
                    d.startDate = fecha_inicial,
                    d.endDate = fecha_final
                }
            },
            columns:[
                {data: "area_id",title: "AREA",className: "all"},
                {data: "fecha",title: "FECHA"},
                {data: "ventas",title: "VENTAS",className: ""},
                {data: "visitantes",title: "VISITANTES",className: ""},
                {data: "potencial",title: "POTENCIAL",className: ""},
                {data: "ticket",title: "# DE TICKETS",className: ""},
                {data: "ticketpromedio",title: "PROMEDIO DE TICKETS",className: ""},
                {data: "conversion",title: "% DE CONVERSION",className: ""},
                {data: "locatario",title: "LOCATARIO",className: ""},
                {data: "centrocomercial",title: "CENTRO COMERCIAL",className: ""}
            ],
            buttons: [
                /*{
                    extend:    'colvis',
                    text:      'Column Visibility',
                    titleAttr: 'Col visibility',
                    className: 'mr-sm-3'
                },*/
                {
                    extend: 'pdfHtml5',
                    text: 'PDF',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1'
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1'
                },
                {
                    extend: 'print',
                    text: 'Print',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-primary btn-sm'
                }
            ],
            // buttons: botones("ventas_por_area","visible",['csv','pdf']),
            language: lenguaje,
            initComplete: function( settings, json ) {
                plotNow(json.chart_info,json.pie_info_area,json.pie_info_locatario);
                $('#total-sales').text("S/. "+json.total_sales);
                $('#avg-ticket').text(json.avg_ticket);
            },
            dom: dom_table(true),
            order: [[1,'desc']]
        });
        var headerTableOne = $(tableOne.table().header());
        var trHtml = '';
        headerTableOne.find('tr:eq(0) th').each(function (i) {
            // console.log(i);
           var title =$(this).text();
           trHtml += '<th><input type="text" class="form-control form-control-sm" placeholder="Buscar ' + title + '" /></th>';
        }).closest('thead').addClass('bg-highlight').append('<tr>'+trHtml+'</tr>').find("th input[type=text]").on('keyup change', function (i) {
            // console.log(i);
            tableOne.column($(this).parent().index()+':visible').search(this.value).draw(); 
        });

        $('#tenant_id').on('select2:select',function (e) {
            // console.log($(this).val());
            var searchTerm = $(this).val(), regex = '\b' + searchTerm + '\b';
            tableOne.columns(8).search(searchTerm,true,false).draw();
        });

        $('#venue_id').on('select2:select',function (e) {
            // console.log($(this).val());
            var searchTerm = $(this).val(), regex = '\b' + searchTerm + '\b';
            tableOne.columns(9).search(searchTerm,true,false).draw();
        });
    });
</script>
@endsection