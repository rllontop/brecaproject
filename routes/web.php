<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (config('app.env') === 'production') {
    URL::forceScheme('https');
}

Route::get('/','DashboardController@index')->name('home');
Route::get('/sale-by-area','DashboardController@sale_by_area')->name('sale.by.area');
Route::get('/indicadores','DashboardController@indicadores')->name('indicadores');
Route::get('/like-by-brand','DashboardController@like_by_brand')->name('like.by.brand');
