var applang = localStorage.getItem('language');
if (applang === undefined) {
    applang = $('html').attr('lang');
}
if (!$.i18n) {
    initApp.loadScript("js/i18n/i18n.js",
        function activateLang() {
            $.i18n.init({
                resGetPath: 'media/data/__lng__.json',
                load: 'unspecific',
                fallbackLng: false,
                lng: applang
            }, function(t) {
                $('[data-i18n]').i18n();
            });
        }
    );
    $('[data-lang]').removeClass('active');
    $('[data-lang=' + applang + ']').addClass('active');
} else {
    i18n.setLng(applang, function() {
        $('[data-i18n]').i18n();
        $('[data-lang]').removeClass('active');
        $('[data-lang=' + applang + ']').addClass('active');
    });
}
var number_of_pages = 10;
var lenguaje = {
    zeroRecords: "Nada que mostrar",
    info: 'Página _PAGE_ de _PAGES_ (_MAX_ registros)',
    infoEmpty: "",
    search: '<div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-search"></i></div></div>',
    paginate: {
        first: '<i class="fal fa-angle-double-left"></i>',
        last: '<i class="fal fa-angle-double-right"></i>',
        next: '<i class="fal fa-angle-right"></i>',
        previous: '<i class="fal fa-angle-left"></i>',
    },
    processing: '<div class="d-flex align-items-center justify-content-center fs-lg"><div class="spinner-border spinner-border-sm text-primary mr-2" role="status"><span class="sr-only"> Cargando...</span></div> Cargando...</div>',
    loadingRecords: "Cargando...",
    lengthMenu: "Mostrar _MENU_ registros",
    infoFiltered: '(_TOTAL_ registros filtrados)'
};
var options_date = {
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    changeYear: true,
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
};
var options_time = {
    timeFormat: "HH:mm",
    defaultTime: ""
};

var botones = (nombreArchivo = null, columnSelector = ":visible", exportar = ["csv"]) => {
    btns = [];
    var namefile = nombreArchivo => {
        // var d = new Date();
        // var n = d.getTime();
        return nombreArchivo + "_" + getFormatDate();
    };
    for (let i = 0; i < exportar.length; i++) {
        var marginright = i == exportar.length - 1 ? "" : "mr-1";
        switch (exportar[i]) {
            case "csv":
                btns.push({
                    extend: "excelHtml5",
                    className: "btn btn-sm btn-default text-success mb-1 " +
                        marginright,
                    autoFilter: true,
                    text: '<i class="fa fa-file-excel-o"></i> Excel',
                    filename: namefile(nombreArchivo),
                    title: null,
                    exportOptions: {
                        // modifier: {
                        //     // page: 'all',
                        //     // title : null
                        // },
                        //:not(:nth-child(1))
                        columns: columnSelector
                    }
                });
                break;
            case "pdf":
                btns.push({
                    extend: "pdfHtml5",
                    className: "btn btn-sm btn-default text-danger mb-1 " +
                        marginright,
                    autoFilter: true,
                    text: '<i class="fa fa-file-pdf-o"></i> Pdf',
                    filename: namefile(nombreArchivo),
                    title: null,
                    // orientation: 'landscape',
                    exportOptions: {
                        modifier: {
                            // page: 'all'
                            // title : null
                        },
                        columns: columnSelector
                    }
                });
                break;
            case "copy":
                btns.push({
                    extend: "copyHtml5",
                    className: "btn btn-sm btn-default text-primary mb-1 " +
                        marginright,
                    autoFilter: true,
                    text: '<i class="fa fa-copy"></i> Copy',
                    filename: namefile(nombreArchivo),
                    title: null,
                    exportOptions: {
                        modifier: {
                            // page: 'all'
                            // title : null
                        },
                        columns: columnSelector
                    }
                });
                break;
            default:
                break;
        }
    }
    return btns;
};

var dom_table = (hasButtons = false, hasPagination = true, isInfoTop = false) => {
    var buttons = hasButtons ? "B" : "";
    var infoTop = isInfoTop ? '<"d-flex"i>' : '';
    lenguaje.info = isInfoTop ? "Total: _MAX_ registros" : 'Página _PAGE_ de _PAGES_ (_MAX_ registros)';
    var infoBottom = isInfoTop ? '' : '<"d-flex flex-grow-1 justify-content-center mb-2 mb-sm-0"<"mr-sm-auto"i>>';
    var pagination = hasPagination ? '<"d-flex flex-grow-1 justify-content-center mb-2 mb-sm-0"<"ml-sm-auto"p>>' : '';
    return ('<"position-relative"r<"d-flex flex-wrap align-items-center mb-3"' + infoTop + buttons + '><t><"d-flex flex-wrap align-items-center justify-content-center"' + infoBottom + pagination + '>>');
};
var statusButtons = (buttons = {}, disabled = false) => {
    if (disabled) {
        for (var button in buttons) {
            // console.log($(button));
            $(button).attr("disabled", true).find("i").removeClass(buttons[button]).addClass("fa-spin fa-refresh");
        }
        $('.dataTables_processing').css({ 'display': 'block' });
    } else {
        for (var button in buttons) {
            // console.log($(button));
            $(button).attr("disabled", false).find("i").removeClass("fa-spin fa-refresh").addClass(buttons[button]);
        }
        $('.dataTables_processing').css({ 'display': 'none' });
    }
};
var getFormatDate = () => {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    return (
        year + "" + month + "" + day + "_" + hours + "" + minutes + "" + seconds
    );
};

var getCurrentPosition = function() {
    if (navigator.geolocation) {
        var settings = {
            enableHighAccuracy: true,
            maximumAge: 0
        };
        return new Promise((resolve, reject) =>
            navigator.geolocation.getCurrentPosition(resolve, reject, settings)
        );
    } else {
        return new Promise(resolve => resolve({}));
    }
};

if (typeof bowser !== 'undefined')
    var browser = JSON.stringify(bowser);

var statusMessage = (contenedor = $("#main"), mensaje = "", estado = "info") => {
    var icon = "";
    switch (estado) {
        case "success":
            icon = "check";
            break;
        case "danger":
            icon = "times";
            break;
        case "warning":
            icon = "warning";
            break;
        case "info":
            icon = "info";
        default:
            icon = "info";
            break;
    }
    contenedor.find("div.alert").remove();
    contenedor.prepend('<div class="alert alert-' + estado + ' fade" style="display: none"><button class="close" data-dismiss="alert">×</button></div>');
    if (typeof mensaje === "string" || mensaje instanceof String || typeof mensaje === "" || (typeof mensaje === "number" && isFinite(mensaje))) {
        contenedor.find(".alert-" + estado).append('<p><i class="fa-fw fa fa-' + icon + '"></i>' + mensaje + "</p>");
    } else {
        $.each(mensaje, function(key, value) {
            contenedor.find(".alert-" + estado).append('<p><i class="fa-fw fa fa-' + icon + '"></i>' + value + "</p>");
        });
    }

    contenedor.find(".alert-" + estado).show().addClass("in");
};

var statusMessagePopup = (mensaje = "", estado = "info") => {
    var icon = "";
    var titulo = "";
    var color = "";
    switch (estado) {
        case "success":
            icon = "check";
            titulo = "CORRECTO!";
            color = "#739e73";
            break;
        case "danger":
            icon = "times";
            titulo = "ERROR!";
            color = "#a90329";
            break;
        case "warning":
            icon = "warning";
            titulo = "ADVERTENCIA!";
            color = "#c79121";
            break;
        case "info":
            icon = "info";
            titulo = "INFO!";
            color = "#3276b1";
        default:
            icon = "info";
            titulo = "INFO!";
            color = "#3276b1";
            break;
    }

    $.smallBox({
        title: "<i class='fa fa-" + icon + "'></i> " + titulo,
        content: "<i class='fa fa-angle-double-right'></i> <i> " + mensaje + "</i>",
        color: color,
        // iconSmall: "fa fa-" + icon + " bounce animated",
        timeout: 10000
    });
};

var showMessagesErrorGeoLocation = error => {
    var msg = null;
    // console.error(error);
    switch (error.code) {
        case error.PERMISSION_DENIED:
            msg = "El usuario rechazó la solicitud de geolocalización.";
            // var position = undefined;
            // send_request_create(modal,position);
            // return;
            break;
        case error.POSITION_UNAVAILABLE:
            msg = "La información de ubicación no está disponible.";
            break;
        case error.TIMEOUT:
            msg =
                "Se agotó el tiempo de espera de la solicitud para obtener la ubicación del usuario.";
            break;
        case error.UNKNOWN_ERROR:
            msg = "Un error desconocido ocurrió.";
            break;
        default:
            msg = "Un error desconocido ocurrió.";
            break;
    }

    return msg;
};

var appendMessagesError = (errors, inputsContainer) => {
    $(".note.note-error").remove();
    $(".state-error").removeClass("state-error");
    $.each(errors, function(key, error) {
        var key_compose = key.split('.')[0];
        var input = inputsContainer.find('input[name="' + key_compose + '"], select[name="' + key_compose + '"]:not([multiple]), select[multiple][name="' + key_compose + '[]"], textarea[name="' + key_compose + '"]');
        input.closest('section').find(".note.note-error").remove();
        input.closest('section').addClass('state-error').append("<div class='note note-error'>" + error[0] + "</div>");
    });
};

var cloneJson = (json) => {
    return JSON.parse(JSON.stringify(json));
}

var empty = (val) => {

    // test results
    //---------------
    // []        true, empty array
    // {}        true, empty object
    // null      true
    // undefined true
    // ""        true, empty string
    // ''        true, empty string
    // 0         false, number
    // true      false, boolean
    // false     false, boolean
    // Date      false
    // function  false

    if (val === undefined)
        return true;

    if (typeof(val) == 'function' || typeof(val) == 'number' || typeof(val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
        return false;

    if (val == null || val.length === 0) // null or 0 length array
        return true;

    if (typeof(val) == "object") {
        // empty object

        var r = true;

        for (var f in val)
            r = false;

        return r;
    }

    return false;
}

var mergeCheckBoxMasterSons = (checkboxMaster, checkboxSons) => {
    checkboxMaster.addEventListener('click', function() {
        var ischecked = this.checked;
        var list = checkboxSons;
        list.forEach((element, index) => {
            element.checked = ischecked;
        });
    });

    checkboxSons.forEach(checkbox => {
        checkbox.addEventListener('click', function() {
            var count = 0;
            checkboxSons.forEach(element => {
                var temp = (element.checked) ? 1 : 0;
                count += temp;
            });
            checkboxMaster.checked = (checkboxSons.length == count);
        });
    });
};

var transitionToOtherPage = (link, container = $.root_) => {
    container.prepend('<div id="page-loading" class="d-flex justify-content-center align-items-center flex-column animated fadeIn duration-2">' +
        '<div class = "position-relative animated fadeInDown p-5 bg-black rounded-lg shadown" style="background-color: rgba(0, 0, 0, .9);">' +
        '<div class = "mb-3" ><h1 class="mt-0 font-weight-bold text-white">Cargando...</h1></div>' +
        '<span class = "load-bar d-flex justify-content-center">' +
        '<span class = "load-bar-item bg-1"> </span>' +
        '<span class = "load-bar-item bg-2 delay-1"> </span>' +
        '<span class = "load-bar-item bg-3 delay-2"> </span>' +
        '<span class = "load-bar-item bg-4 delay-3"> </span>' +
        '</span>' +
        '</div>' +
        '</div>');
    setTimeout(() => {
        window.location.href = link.attr('href');
    }, 500);
}

$(document).ready(function() {

});