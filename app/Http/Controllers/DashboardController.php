<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $tenants = DB::select("SELECT id,brand as locatario FROM tenants;");
        $venues = DB::select("SELECT id,name as centrocomercial FROM venues;");
        return view('index', compact('tenants', 'venues'));
    }

    public function sale_by_area(Request $request)
    {
        $data = collect(DB::select("SELECT * from (select valuesg.area_id,valuesg.fecha as fecha,valuesg.ventas,
        num_ingress as visitantes,num_tranit as potencial,
        valuesg.ticket,valuesg.ticketpromedio,
        round(((valuesg.ticket*100)/num_ingress),2) as conversion,valuesg.te as locatario ,valuesg.ve as centrocomercial
        from counts v,
        (select
        b.area_id,date(a.date) as fecha
        ,round(sum(a.num_sales),2) as ventas
        ,sum(a.num_transactions) as ticket
        ,avg(a.num_transactions) as ticketpromedio
        ,c.tenant_id as te,c.venue_id as ve
        from sales a
        left join ss_tenants b on a.ss_tenant_id=b.ss_tenant_id
        left join areas c on c.id=b.area_id
        where b.area_id IS NOT NULL
        group by b.area_id,date(a.date)
        ,c.tenant_id,c.venue_id
        ) as valuesg
        where
        v.area_id=valuesg.area_id
        and
        date(date)=valuesg.fecha) as v
        where v.fecha between ? and ?",[$request->get('startDate'),$request->get('endDate')]));

        $grouppedDate = $data->groupBy('fecha');

        $chart_info = $grouppedDate->map(function ($item,$key){
            return [strtotime($key) * 1000, round(collect($item)->sum('ventas'))];
        });

        $grouppedArea = $data->groupBy('area_id');
        $grouppedlocatario = $data->groupBy('locatario');

        $colors = collect(['success','danger','warning','fusion','primary','info']);

        $pie_info_area = $grouppedArea->map(function ($item,$key) use($colors){
            return ['label' => $key , 'data' => round(collect($item)->sum('ventas')), 'color' => $this->randomHex() ];
        });

        $pie_info_locatario = $grouppedlocatario->map(function ($item,$key) use($colors){
            return ['label' => $key , 'data' => round(collect($item)->sum('ventas')), 'color' => $this->randomHex() ];
        });

        return response()->json([
            'data' => $data->toArray(), 
            'date_range' => $request->all(),
            'chart_info' => $chart_info->values()->toArray(),
            'pie_info_area' => (($pie_info_area->values())->sortByDesc('data')->values())->take(10)->toArray(),
            'pie_info_locatario' => (($pie_info_locatario->values())->sortByDesc('data')->values())->take(10)->toArray(),
            'total_sales' => round($data->sum('ventas'),2),
            'avg_ticket' => round($data->avg('ticketpromedio'),2)
            ]);
    }

    public function randomHex() {
        $chars = 'ABCDEF0123456789';
        $color = '#';
        for ( $i = 0; $i < 6; $i++ ) {
           $color .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $color;
    }

    public function indicadores()
    {
        $tenants = DB::select("SELECT id,brand as locatario FROM tenants;");
        // $venues = DB::select("SELECT id,name as centrocomercial FROM venues;");
        return view('indicadores', compact('tenants'));
    }

    public function like_by_brand(Request $request)
    {
        
        $data = ($request->get('brand') != 'TODOS' && $request->get('brand')) ? collect(DB::select("SELECT SUBSTR(brand, 1, 20) as locatario, counta from comp_bybrand_likes where brand = ?",[$request->get('brand')])) : collect(DB::select("SELECT SUBSTR(brand, 1, 20) as locatario, counta from comp_bybrand_likes"));
        
        $data_pie = collect(DB::select("select email,gender,YEAR(CURDATE()) - YEAR(date_of_birth) AS edad
        from visitors;"));
        
        $key_gender = $data_pie->groupBy('gender');
        $key_edad = $data_pie->groupBy('edad');

        $colors = collect(['success','danger','warning','fusion','primary','info']);

        $pie_info_gender = $key_gender->map(function ($item,$key) use($colors){
            return ['label' => $key , 'data' => collect($item)->count(), 'color' => $this->randomHex()];
        });
        $pie_info_edad = $key_edad->map(function ($item,$key) use($colors){
            return ['label' => $key , 'data' => collect($item)->count(), 'color' => $this->randomHex()];
        });

        $pie_info_likes = $data->map(function ($item,$key) use($colors){
            $tmp = collect($item);
            return ['label' => substr($tmp->get('locatario'),0,4) , 'data' => $tmp->get('counta'), 'color' => $this->randomHex()];
        });

        return response()->json([
            'data' => $data->toArray(),
            'date_range' => $request->all(),
            'pie_info_genero' => $pie_info_gender->values()->toArray(),
            'pie_info_edad' =>  (($pie_info_edad->values())->sortByDesc('data')->values())->take(10)->toArray(),
            'pie_info_likes' =>  (($pie_info_likes->values())->sortByDesc('data')->values())->take(5)->toArray(),
            'total_sales' => collect()->sum('ventas'),
            'avg_ticket' => round(collect()->avg('ticketpromedio'),2)
            ]); 
    }
}

