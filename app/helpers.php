<?php

use Illuminate\Support\Str;

if (!function_exists('getFieldsAttribute')) {
  function getFieldsAttribute($value)
  {
    return (array) json_decode($value, true);
  }
}

if (!function_exists('rest_api')) {
  function rest_api($array_data = "", $url, $method, $timeout = 120, $http_header = [])
  {
    $curl = curl_init();

    $array_data['env'] = config('app.env');
    $json_data = is_array($array_data) ? json_encode($array_data, true) : $array_data;

    $http_header = empty($http_header) ? array_merge($http_header, [
      "Content-Type: application/json",
      "Cache-Control: no-cache",
    ]) : array_merge($http_header, ["Cache-Control: no-cache"]);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => $timeout,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_POSTFIELDS => $json_data,
      CURLOPT_HTTPHEADER => $http_header,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    }
    return $response;
  }
}

if (!function_exists('rest_api_token')) {
  function rest_api_token()
  {
    $credencials = [
      "grant_type" => "client_credentials",
      "client_id" => 7,
      "client_secret" => "bZtI2r4liDPMoSo1MLow3LrDEVjDYBBzUCSpNVOt",
      "scope" => ""
    ];

    $token = json_decode(rest_api($credencials, "https://apirest.sbperu.com/oauth/token", "POST"), true);

    return $token;
  }
}

if (!function_exists('get_url_api_rest')) {
  function get_url_api_rest()
  {
    return "https://apirest.sbperu.com/v2/smartapp/net";
  }
}

if (!function_exists('setActive')) {
  function setActive($routeName = '', $className = 'active')
  {
    if (is_array($routeName)) {
      for ($i = 0; $i < count($routeName); $i++) {
        if (request()->routeIs($routeName[$i]))
          return  $className;
      }
      return '';
    } else {
      return request()->routeIs($routeName) ? $className : '';
    }
  }
}

if (!function_exists('print_url_img_sbperu_page')) {
  function print_url_img_sbperu_page($path)
  {
      return 'https://sbperu.com/app/resources/media/img' . Str::start($path, '/');
  }
}